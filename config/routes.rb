Rails.application.routes.draw do
  resources :account_activations, only: [:edit]
  resources :microposts, only: [:create, :destroy]

  root 'static_pages#home'
  get 'users/show'
  get 'static_pages/home'
  get 'static_pages/help'

  get '/signup',  to: 'users#signup'
  get '/login', to: 'sessions#signin'
  post '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  
  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :relationships, only: [:create, :destroy]
end
