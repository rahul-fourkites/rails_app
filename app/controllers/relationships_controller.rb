class RelationshipsController < ApplicationController

def create 
	@user = User.find_by(id: params[:followed_id])
	logged_in_user.follow(@user)
	respond_to do |format|
		format.html {redirect_to @user}
		format.js
	end
end

def destroy
	@user = Relationship.find(params[:id]).followed
    logged_in_user.unfollow(@user)
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
end

end
