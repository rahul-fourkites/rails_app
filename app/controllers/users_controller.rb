class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy,
                                        :following, :followers]

  def welcome
    render html: "welcome to app"
  end

  def index
  	@users = User.paginate(page: params[:page])
  end

  def show
    @user = User.find(params[:id])
    # unless logged_in @user
    #   redirect_to login_path
    # else
      @microposts = @user.microposts.paginate(page: params[:page])
    # end
  end

  def signup
  	@user = User.new
  end

  def create 
  	@user = User.new(user_params)
  	if @user.save
  		redirect_to @user
  	else
  		render 'signup'
  	end
  end

  def following
    @title = "Following"
    @user  = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = "Followers"
    @user  = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end

  private
  	def user_params
  		params.require(:user).permit(:name, :email, :password, :password_confirmation);
  	end
end
