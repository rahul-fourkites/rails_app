class MicropostsController < ApplicationController
	before_action :logged_in_user, only: [:create, :destroy]
	before_action :correct_user, only: [:destroy]

	def create
		if logged_in_user
			@micropost = logged_in_user.microposts.build(micropost_params)
			if @micropost.save
				redirect_to root_url
			else
				@feed_items = []
				render 'static_pages/home'
			end
		end
	end

	def destroy
		correct_user.destroy
		redirect_to request.referrer || root_url
	end

	def correct_user
		@micropost = logged_in_user.microposts.find_by(id: params[:id])
		if @micropost.nil?
			redirect_to root_url
		else
			return @micropost
		end
	end

	private
		def micropost_params
			params.require(:micropost).permit(:content)
		end

end
