class StaticPagesController < ApplicationController
  def home
  	if logged_in_user
  		@micropost = logged_in_user.microposts.build
  		@feed_items = logged_in_user.feed.paginate(page: params[:page])
  	end
  end

  def help
  end
end
