class SessionsController < ApplicationController
  def signin
  end

  def create 
  	user = User.find_by(email:params[:session][:email].downcase)
  	if user && user.authenticate(params[:session][:password])
  		log_in user
      current_user
  		redirect_to user
  	else
  		render 'signin'
  	end
  end

  def destroy
  	if log_in?
  		log_out
  		redirect_to root_path
  	end
  end
end
